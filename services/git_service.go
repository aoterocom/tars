package services

import (
	"errors"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
	"gitlab.com/aoterocom/tars/models"
	"log"
	"net/url"
	"os"
	"strings"
	"time"
)

type GitService struct {
	repoUrl     *string
	repository  *git.Repository
	localPath   *string
	Credentials *models.GitCredentials
}

func NewGitService(repoUrl string) *GitService {
	split := strings.Split(repoUrl, "/")
	folderName := url.PathEscape(split[len(split)-1])
	tempDir := os.TempDir() + "tars/" + folderName + "/"
	return &GitService{repoUrl: &repoUrl, localPath: &tempDir}
}

func (gs *GitService) Clone() error {
	split := strings.Split(*gs.repoUrl, "/")
	folderName := url.PathEscape(split[len(split)-1])
	*gs.localPath = os.TempDir() + "tars/" + folderName + "/"

	if _, err := os.Stat(*gs.localPath); err == nil || os.IsExist(err) {
		log.Println("directory " + *gs.localPath + " already exists. Clone skipped")
		gs.repository, err = git.PlainOpen(*gs.localPath)
		if err != nil {
			return err
		}
	} else {
		log.Println("clonning git repository:  " + *gs.repoUrl)
		err = os.MkdirAll(*gs.localPath, 0777)
		if err != nil {
			return err
		}

		gs.repository, err = git.PlainClone(*gs.localPath, false, &git.CloneOptions{
			URL:      *gs.repoUrl,
			Progress: os.Stdout,
			Auth: &http.BasicAuth{
				Username: gs.Credentials.Username,
				Password: gs.Credentials.Token,
			},
		})
		if err != nil {
			return err
		}
		log.Println("successfully cloned in  " + *gs.localPath)
	}
	return nil
}

func (gs *GitService) CreateBranch(branchName string) error {
	w, err := gs.repository.Worktree()
	if err != nil {
		return errors.New("error: getting the worktree: " + err.Error())
	}
	err = w.Checkout(&git.CheckoutOptions{
		Create: true,
		Branch: plumbing.ReferenceName("refs/heads/" + branchName),
	})

	if err != nil {
		return errors.New("error: checking out " + branchName + " branch: " + err.Error())
	}

	return nil
}

func (gs *GitService) LocalPath() string {
	return *gs.localPath
}

func (gs *GitService) SetCredentials(gitCredentials *models.GitCredentials) {
	gs.Credentials = gitCredentials
}

func (gs *GitService) Commit(commitMessage string) error {
	if gs.Credentials == nil {
		return errors.New("error: Credentials have not been provided")
	}

	w, err := gs.repository.Worktree()
	if err != nil {
		return errors.New("error: getting the worktree: " + err.Error())
	}
	status, err := w.Status()
	if err != nil {
		return errors.New("error: getting git status: " + err.Error())
	}

	if !status.IsClean() {
		_, err = w.Add(".")
		if err != nil {
			return errors.New("error: adding git files: " + err.Error())
		}
		commit, err := w.Commit(commitMessage, &git.CommitOptions{
			Author: &object.Signature{
				Name:  gs.Credentials.AuthorName,
				Email: gs.Credentials.AuthorEmail,
				When:  time.Now(),
			},
		})
		if err != nil {
			return errors.New("error: when commit: " + err.Error())
		}
		log.Println("committed: " + commit.String())
	} else {
		log.Println("no changes to commit")
	}
	return nil
}

func (gs *GitService) Push() error {
	if gs.Credentials == nil {
		return errors.New("error: Credentials have not been provided")
	}

	w, err := gs.repository.Worktree()
	if err != nil {
		return errors.New("error: getting the worktree: " + err.Error())
	}
	status, err := w.Status()
	if err != nil {
		return errors.New("error: getting git status: " + err.Error())
	}

	if status.IsClean() {
		pushOptions := git.PushOptions{
			Auth: &http.BasicAuth{
				Username: gs.Credentials.Username,
				Password: gs.Credentials.Token,
			},
		}
		err = gs.repository.Push(&pushOptions)
		if err != nil {
			return errors.New("error: getting git status: " + err.Error())
		}
	}
	return nil
}
