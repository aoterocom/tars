package models

import (
	"errors"
	"os"
)

type GitCredentials struct {
	AuthorName  string
	AuthorEmail string
	Username    string
	Token       string
}

func NewGitCredentials(authorName string, authorEmail string) *GitCredentials {
	return &GitCredentials{AuthorName: authorName, AuthorEmail: authorEmail}
}

func NewEnvGitCredentials() (*GitCredentials, error) {
	authorName := os.Getenv("GIT_AUTHOR_NAME")
	authorEmail := os.Getenv("GIT_AUTHOR_EMAIL")
	gitToken := os.Getenv("GIT_TOKEN")
	gitUsername := os.Getenv("GIT_USERNAME")
	if authorName == "" || authorEmail == "" {
		return nil, errors.New("GIT_AUTHOR_NAME and GIT_AUTHOR_EMAIL environment variables must be set")
	}
	if gitToken == "" || gitUsername == "" {
		return nil, errors.New("GIT_TOKEN and GIT_USERNAME environment variables must be set")
	}
	return &GitCredentials{AuthorName: authorName, AuthorEmail: authorEmail, Token: gitToken, Username: gitUsername}, nil
}
