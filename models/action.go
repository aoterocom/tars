package models

import (
	"bufio"
	"fmt"
	"os/exec"
)

type Action struct {
	Name string            `yaml:"name,omitempty"`
	Env  map[string]string `yaml:"env,omitempty"`
	Job  Job               `yaml:"jobs"`
}
type Job struct {
	Steps []*Step `yaml:"steps"`
}
type Step struct {
	Name         *string `yaml:"name"`
	Instructions *string `yaml:"run"`
}

func NewAction(name string) *Action {
	return &Action{
		Name: name}
}

func (a *Action) Exec() {
	fmt.Println("=== EXECUTE " + a.Name)
	for _, step := range a.Job.Steps {

		fmt.Println("  -> STEP " + *step.Name)
		cmd := exec.Command("bash", "-c", *step.Instructions)
		stdout, err := cmd.StdoutPipe()
		if err != nil {
			panic(err)
		}

		err = cmd.Start()
		if err != nil {
			panic(err)
		}

		scanner := bufio.NewScanner(stdout)
		scanner.Split(bufio.ScanLines)
		for scanner.Scan() {
			m := scanner.Text()
			fmt.Println("       " + m)
		}

		err = cmd.Wait()
		if err != nil {
			panic(err)
		}
	}
}
