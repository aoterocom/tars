package executors

import (
	"github.com/spf13/cobra"
	. "gitlab.com/aoterocom/tars/helpers"
	"gitlab.com/aoterocom/tars/models"
	"gitlab.com/aoterocom/tars/services"
	"log"
	"os"
	"path/filepath"
)

func RootExec(cmd *cobra.Command, args []string) {

	// Grab CLI arguments
	flags := cmd.Flags()
	log.Println("Checking arguments...")
	workflow, err := flags.GetString("workflow")
	CheckErr(err)
	repoUrl, err := flags.GetString("repo-url")
	CheckErr(err)

	if workflow == "" {
		log.Fatalln("workflow needs to be defined")
	}

	if repoUrl == "" {
		log.Fatalln("repo URL needs to be defined")
	}

	// Clone repo
	log.Println("Cloning repo...")
	var gitService *services.GitService
	gitService = services.NewGitService(repoUrl)
	credentials, err := models.NewEnvGitCredentials()
	CheckErr(err)
	gitService.SetCredentials(credentials)
	err = gitService.Clone()
	CheckErr(err)

	// Create Branch
	log.Println("Creating new branch " + "tars/" + workflow)
	err = gitService.CreateBranch("tars/" + workflow)
	CheckErr(err)

	// Create workflow dir if doesn't exists
	log.Println("Checking workflow dirs")
	tarsPath, err := os.Getwd()
	workflowsPath := gitService.LocalPath() + ".github/workflows/"
	workflowsRecoveryPath := gitService.LocalPath() + ".github/workflows-rec/"

	if _, err := os.Stat(workflowsPath); !os.IsNotExist(err) {
		log.Println("Workflows directory exists. Backing up repos to '.github/workflows-rec/'")
		_, err = Copy(workflowsPath, workflowsRecoveryPath)
		CheckErr(err)
		err = gitService.Commit("TARS: back up workflows")
		CheckErr(err)
	} else {
		log.Println("Workflows directory not found. Creating...")
		err = os.MkdirAll(workflowsPath, 0777)
		CheckErr(err)
	}

	log.Println("Injecting tars workflow '" + workflow + "'...")

	_, err = Copy(filepath.Join(tarsPath, workflow), filepath.Join(workflowsPath, "tars.yml"))
	CheckErr(err)

	err = os.Chdir(gitService.LocalPath())
	CheckErr(err)

	// Commit and push workflow addition
	err = gitService.Commit("TARS: add action")
	CheckErr(err)
	err = gitService.Push()
	CheckErr(err)
}
