package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/aoterocom/tars/cmd/executors"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "tars",
	Short: "Hi, i'm TARS. I'm here to enhance your developer experience",
	Long: `TARS is an automator that provides you support to iterate
actions over multiple git repositories pushing the results`,
	Run: func(cmd *cobra.Command, args []string) {
		executors.RootExec(cmd, args)
	},
}

func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	rootCmd.Flags().StringP("workflow", "w", "", "path to your Workflow")
	rootCmd.Flags().StringP("repo-url", "r", "", "git repository url")
}
