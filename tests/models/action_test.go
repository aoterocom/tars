package models

import (
	"gitlab.com/aoterocom/tars/models"
	"testing"
)

func TestAction(t *testing.T) {
	action := models.NewAction("Test Action")
	name := "First Step"
	instructions := "echo \"Hi! :)\""
	step := models.Step{
		Name:         &name,
		Instructions: &instructions,
	}
	action.Job.Steps = append(action.Job.Steps, &step)
	action.Exec()
}
