# TARS

90% Honesty, 100% Developer Experience

## What it does?

Injects a Github workflow into a repository, to trigger its execution once pushed and backing up any other workflow.

## Usage

Compile with:

`go build`

CLI Usage:

```
tars [flags]

Flags:
  -w, --workflow string   path to your Workflow
  -h, --help                help for tars
```