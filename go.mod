module gitlab.com/aoterocom/tars

go 1.15

require (
	github.com/go-git/go-git/v5 v5.3.0
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1 // indirect
	github.com/stretchr/testify v1.4.0
	gopkg.in/yaml.v2 v2.4.0
)
